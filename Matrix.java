package com.company;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Ja on 19.01.2016.
 */
public class Matrix {

    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public Matrix(int r,int c){
        collumn = c;
        row = r;
        matrix = new Double[r*c];
        for(int i=0;i<matrix.length;++i){
            matrix[i] = new Double(0.0);
        }
    }
    
    public Double get(int r,int c){
        if(r<0 && r>=row) {
            throw new IndexOutOfBoundsException("Bad row index");
        }
        if(c<0 && c>=collumn){
            throw new IndexOutOfBoundsException("Bad collumn index");
        }
        return matrix[r * collumn + c];

    }

    public Double parralerGet(int r,int c){
        if(r<0 && r>=row) {
            throw new IndexOutOfBoundsException("Bad row index");
        }
        if(c<0 && c>=collumn){
            throw new IndexOutOfBoundsException("Bad collumn index");
        }
        try {
            readWriteLock.readLock().lock();
            return matrix[r * collumn + c];
        }finally {
            readWriteLock.readLock().unlock();
        }
    }

    public void set(int r,int c,Double value){
        if(r<0 && r>=row) {
            throw new IndexOutOfBoundsException("Bad row index");
        }
        if(c<0 && c>=collumn){
            throw new IndexOutOfBoundsException("Bad collumn index");
        }
        readWriteLock.writeLock().lock();
        matrix[r * collumn + c] = value;
    }

    public void parralerSet(int r,int c,Double value){
        if(r<0 && r>=row) {
            throw new IndexOutOfBoundsException("Bad row index");
        }
        if(c<0 && c>=collumn){
            throw new IndexOutOfBoundsException("Bad collumn index");
        }
        try {
            readWriteLock.writeLock().lock();
            matrix[r * collumn + c] = value;
        }finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public Matrix identity(){
        for(int i=0;i<matrix.length;++i){
            matrix[i]=0.0;
        }
        int s;
        if(row<collumn){
            s= row;
        }else{
            s=collumn;
        }
        for(int j=0;j<s;++j){
            matrix[j*collumn+j] = 1.0;
        }
        return this;
    }

    public Matrix mutiply(Double c){
        Matrix A = new Matrix(row,collumn);
        for(int i=0;i<matrix.length;++i){
            A.matrix[i]=c*matrix[i];
        }
        return A;
    }

    public Matrix parallerMutiply(Double c){
            Multipler[] multiplers = new Multipler[matrix.length];
            for (int i = 0; i < matrix.length; ++i) {
                Double a = matrix[i];
                multiplers[i] = new Multipler(c, a);
                multiplers[i].start();
            }
            for (int i = 0; i < matrix.length; ++i) {
                try {
                    multiplers[i].join();
                } catch (InterruptedException e) {
                }
            }
            Matrix A = new Matrix(row,collumn);
            for (int i = 0; i < matrix.length; ++i) {
                A.matrix[i] = multiplers[i].get();
            }
            return A;
    }

    public Matrix multiply(Matrix B){
        if(collumn == B.row) {
            Matrix C = new Matrix(row, B.collumn);
            Matrix A = this;
            for(int i=0;i<C.row;++i){
                for(int j=0;j<C.collumn;++j){
                    Double s = 0.0;
                    for(int k=0;k<collumn;++k){
                        s+=A.get(i,k)*B.get(k,j);
                    }
                    C.set(i,j,s);
                }
            }
            return C;
        }else {
            throw new IndexOutOfBoundsException("Different sizes of matrices");
        }
    }

    public Matrix parallerMultiply(Matrix B){
        if(collumn == B.row) {
            Matrix C = new Matrix(row, B.collumn);
            Matrix A = this;

            for(int i=0;i<C.row;++i){
                for(int j=0;j<C.collumn;++j){
                    Double s = 0.0;
                    Double[] doubles = new Double[collumn];
                    for(int k=0;k<collumn;++k){
                        doubles[k]=A.parralerGet(i,k)*B.parralerGet(k,j);
                    }
                    final int numOfThread = Runtime.getRuntime().availableProcessors();
                    final ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThread);
                    s = forkJoinPool.invoke(new RecusiveSum(doubles,0,collumn -1));
                    C.set(i,j,s);
                }
            }
            return C;
        }else {
            throw new IndexOutOfBoundsException("Different sizes of matrices");
        }
    }

    public MyVector multily(MyVector V){
        if(collumn != V.getSize()){
            throw new IndexOutOfBoundsException("Different sizes of matrix and vector");
        }
        MyVector C = new MyVector(V.getSize());
        Matrix A = this;
        for(int i=0;i<row;++i){
            Double s=0.0;
            for(int j=0;j<collumn;++j){
                s+=A.get(i,j)*V.get(j);
            }
            C.set(i,s);
        }
        return C;
    }

    public MyVector parallerMultily(MyVector V){
        if(collumn != V.getSize()){
            throw new IndexOutOfBoundsException("Different sizes of matrix and vector");
        }
        MyVector C = new MyVector(V.getSize());
        Matrix A = this;
        for(int i=0;i<row;++i){
            Double s=0.0;
            Double[] doubles = new Double[collumn];
            int numOfThread = Runtime.getRuntime().availableProcessors();
            ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThread);
            for(int j=0;j<collumn;++j){
                doubles[j]=A.parralerGet(i,j)*V.parralerGet(j);
            }
            s = forkJoinPool.invoke(new RecusiveSum(doubles, 0, collumn - 1));
            C.set(i,s);
        }
        return C;
    }

    public Matrix add(Double c){
        Matrix A = new Matrix(row,collumn);
        for(int i=0;i<matrix.length;++i) {
            A.matrix[i] = matrix[i]+c;
        }
        return A;
    }

    public Matrix parallerAdd(Double c){
        Adder[] adder = new Adder[matrix.length];
        for(int i=0;i<matrix.length;++i) {
            adder[i] = new Adder(matrix[i],c);
            adder[i].start();
        }
        for(int i=0;i<matrix.length;++i){
            try{
                adder[i].join();
            }catch (InterruptedException e){}
        }
        Matrix A = new Matrix(row,collumn);
        for(int i=0;i<matrix.length;++i){
            A.matrix[i] = adder[i].get();
        }
        return A;
    }

    public Matrix add(Matrix B){
        Matrix A = this;
        Matrix C = new Matrix(row,collumn);
        if((A.collumn == B.collumn) && (A.row == B.row)){
            for(int i=0;i<A.matrix.length;++i){
                C.matrix[i] = A.matrix[i]+B.matrix[i];
            }
        }
        return C;
    }

    public Matrix parallerAdd(Matrix B){
        Matrix A = new Matrix(row,collumn);
        if((A.collumn == B.collumn) && (A.row == B.row)){
            Adder[] adders = new Adder[matrix.length];
            for(int i=0;i<matrix.length;++i){
                adders[i] = new Adder(matrix[i],B.matrix[i]);
                adders[i].start();
            }
            for(int i=0;i<matrix.length;++i){
                try{
                    adders[i].join();
                }catch (InterruptedException e){}
            }
            for(int i=0;i<matrix.length;++i){
                A.matrix[i] = adders[i].get();
            }
        }
        return A;
    }

    public Matrix sub(Double c){
        Matrix A = new Matrix(row,collumn);
        for(int i=0;i<matrix.length;++i) {
            A.matrix[i] = matrix[i]- c;
        }
        return A;
    }

    public Matrix parallerSub(Double c){
        Substracter[] substracters = new Substracter[matrix.length];
        for(int i=0;i<matrix.length;++i) {
            substracters[i] = new Substracter(matrix[i],c);
            substracters[i].start();
        }
        for(int i=0;i<matrix.length;++i) {
            try{
                substracters[i].join();
            }catch (InterruptedException e){}
        }
        Matrix A = new Matrix(row,collumn);
        for(int i=0;i<matrix.length;++i) {
            A.matrix[i] = substracters[i].get();
        }
        return A;
    }

    public Matrix sub(Matrix B){
        Matrix A = this;
        Matrix C = new Matrix(row,collumn);
        if((A.collumn == B.collumn) && (A.row == B.row)){
            for(int i=0;i<A.matrix.length;++i){
                C.matrix[i] = A.matrix[i]-B.matrix[i];
            }
        }
        return C;
    }

    public Matrix parallerSub(Matrix B){
        Matrix A = this;
        Matrix C = new Matrix(row,collumn);
        if((A.collumn == B.collumn) && (A.row == B.row)){
            Substracter[] substracters = new Substracter[matrix.length];
            for(int i=0;i<A.matrix.length;++i){
                substracters[i] = new Substracter(A.matrix[i],B.matrix[i]);
                substracters[i].start();
            }
            for(int i=0;i<A.matrix.length;++i){
                try{
                    substracters[i].join();
                }catch (InterruptedException e){}
            }
            for(int i=0;i<A.matrix.length;++i){
                C.matrix[i] = substracters[i].get();
            }
        }
        return C;
    }

    public Matrix transpose(){
        Matrix A = new Matrix(collumn,row);
        Matrix B = this;
        for(int i=0;i<row;++i){
            for(int j=0;j<collumn;++j){
                A.set(j,i,B.get(i,j));
            }
        }
        return A;
    }

    public void printMatrx(){
        for(int i=0;i<row;++i){
            System.out.print("{ ");
            for(int j=0;j<collumn;++j){
                System.out.print(matrix[i*collumn+j] + ", ");
            }
            System.out.println("}");
        }
    }

    private Double[] matrix;

    public int getRow() {
        return row;
    }

    private int row;

    public int getCollumn() {
        return collumn;
    }

    private int collumn;
}
