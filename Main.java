/*package com.company;

public class Main {

    public static void main(String[] args) {

        /*Matrix matrix = new Matrix(3,3);
        MyVector myVector = new MyVector(3);
        matrix.set(0,0,4.0);
        matrix.set(0,1,-2.0);
        matrix.set(0,2,1.0);
        matrix.set(1,0,8.0);
        matrix.set(1,1,-1.0);
        matrix.set(1,2,1.0);
        matrix.set(2,0,1.0);
        matrix.set(2,1,3.0);
        matrix.set(2,2,-1.0);

        myVector.set(0,-4.0);
        myVector.set(1,-2.0);
        myVector.set(2,6.0);
        Conjugate conjugate = new Conjugate(matrix,myVector);
        MyVector x = conjugate.calc();
        x.printVector();*/
 /*   }
}*/


package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {


    public static LinkedList<Double> readIn(){
        LinkedList<Double> matrix=new LinkedList<>();
        System.out.println("Give size of sq matrix:  ");
        Scanner sc = new Scanner(System.in);
        int size;
        size=sc.nextInt();

        for(int i=0;i<size*size;i++){
            System.out.println("Give me next double using , :");
            double temp;
            temp=sc.nextDouble();
            matrix.add(temp);
        }
        //print matrix
        /*
        for(int i=0;i<matrix.size();i++){
            System.out.print(i%size==0?("\n[ "+matrix.get(i).doubleValue()):(" "+matrix.get(i).doubleValue())+(i%size==size-1?" ]":""));

        }
        System.out.println();*/
        return matrix;

    }
    public static Matrix giveMatrix(){
        LinkedList<Double> we=new LinkedList<>();
        we=readIn();
        Matrix matrix=new Matrix((int)Math.sqrt(we.size()),(int)Math.sqrt(we.size()));

        for(int j=0;j<matrix.getRow();j++){
            for(int i=0;i<matrix.getCollumn();i++){
                matrix.set(j, i, we.get(j*(int)Math.sqrt(we.size())+i).doubleValue());
            }
        }
        return matrix;
    }
    public static void readByHand(){
        Matrix matrix=giveMatrix();
        ///przykładowe menu
        Scanner s = new Scanner(System.in);
        boolean isAlive=true;

        while (isAlive) {
            int q;
            System.out.println("What you want?:\n1.Print Matrix\n2.Multiply by\nMatrix add Matrix");
            q=s.nextInt();
            switch (q) {
                case 1:
                    System.out.println("***\tMatrix\t***");
                    matrix.printMatrx();
                    break;
                case 2:
                    System.out.println("***\tMatrix mutiply by (give the number) \t***");
                    double w=s.nextDouble();
                    matrix.parallerMutiply(new Double(w));
                    break;
                case 3:
                    System.out.println("***\tMatrix add Matrix\t***");
                    System.out.println("New matrix (1) or the same(2) or from File(3)");
                    q=s.nextInt();
                    switch (q) {
                        case 1:
                            Matrix matrix2=giveMatrix();
                            matrix.add(matrix2);
                            break;
                        case 2:
                            Matrix matrix1 = new Matrix(matrix.getRow(),matrix.getCollumn());
                            matrix1 = matrix;
                            matrix.add(matrix1);
                            break;
                        case 3:
                            Matrix matrix3=giveMatrixFromFile();
                            matrix.add(matrix3);

                            break;
                        default:
                            break;
                    }
                    Matrix matrix1 = new Matrix(matrix.getRow(),matrix.getCollumn());
                    matrix1 = matrix;
                    matrix.add(matrix1);
                    break;
                default:
                    System.out.println("End of program BB");
                    isAlive=false;
                    break;
            }
        }



    }
    public static Matrix giveMatrixFromFile(){
        LinkedList<Double> we=new LinkedList<>();
        we=readFromFileFuncion();
        Matrix matrix=new Matrix((int)Math.sqrt(we.size()),(int)Math.sqrt(we.size()));

        for(int j=0;j<matrix.getRow();j++){
            for(int i=0;i<matrix.getCollumn();i++){
                matrix.set(j, i, we.get(j*(int)Math.sqrt(we.size())+i).doubleValue());
            }
        }
        return matrix;
    }
    public static LinkedList<Double> readFromFileFuncion(){
        System.out.println("Give the path to file:");
        Scanner s = new Scanner(System.in);
        String path=s.nextLine();
        Scanner in = null;
        try {
            in = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            System.out.println("BadFileName");
            e.printStackTrace();
        }
        String q;
        LinkedList<Double> fromFile=new LinkedList<>();
        while (null!=(q=in.nextLine())){
            // System.out.println(q);
            fromFile.add(Double.valueOf(q));
            if(!in.hasNextLine())
                break;
        }
        return fromFile;
    }
    private static void readFromFile() {
        Matrix matrix=giveMatrixFromFile();
        ///przykładowe menu
        Scanner s = new Scanner(System.in);
        boolean isAlive=true;

        while (isAlive) {
            int q;
            System.out.println("What you want?:\n1.Print Matrix\n2.Multiply by\nMatrix add Matrix");
            q=s.nextInt();
            switch (q) {
                case 1:
                    System.out.println("***\tMatrix\t***");
                    matrix.printMatrx();
                    break;
                case 2:
                    System.out.println("***\tMatrix mutiply by (give the number) \t***");
                    double w=s.nextDouble();
                    matrix.parallerMutiply(new Double(w));
                    break;
                case 3:
                    System.out.println("***\tMatrix add Matrix\t***");
                    System.out.println("New matrix (1) or the same(2) or from File(3)");
                    q=s.nextInt();
                    switch (q) {
                        case 1:
                            Matrix matrix2=giveMatrix();
                            matrix.add(matrix2);
                            break;
                        case 2:
                            Matrix matrix1 = new Matrix(matrix.getRow(),matrix.getCollumn());
                            matrix1 = matrix;
                            matrix.add(matrix1);
                            break;
                        case 3:
                            Matrix matrix3=giveMatrixFromFile();
                            matrix.add(matrix3);

                            break;
                        default:
                            break;
                    }
                    Matrix matrix1 = new Matrix(matrix.getRow(),matrix.getCollumn());
                    matrix1 = matrix;
                    matrix.add(matrix1);
                    break;
                default:
                    System.out.println("End of program BB");
                    isAlive=false;
                    break;
            }
        }


    }
    public static void readVektor(){
        Scanner s = new Scanner(System.in);
        int size;
        size=s.nextInt();
        MyVector vec=new MyVector(size);
        for(int i=0; i<size;i++){
            double temp;
            temp=s.nextDouble();
            vec.set(i, temp);
        }
        vec.printVector();
    }
    public static void demo(){

        //to do
        /*int s = 5;
        MyVector myVector = new MyVector(s);
        MyVector myVector1 = new MyVector(s);

        for(int i=0;i<s;++i){
            myVector.set(i,new Double(1));
            myVector1.set(i,new Double(1));
        }
        Matrix matrix1 = myVector.parallerOuterProduct(myVector1);
        System.out.println("***\tMatrix\t***");
        matrix1.printMatrx();
        System.out.println("***\tMatrix mutiply by 2\t***");
        matrix1.parallerMutiply(new Double(2.0));
        matrix1.printMatrx();
        Matrix matrix2 = new Matrix(s,s);
        matrix2 = matrix1;
        System.out.println("***\tMatrix add Matrix\t***");
        matrix1.add(matrix1);
        matrix1.printMatrx();*/

        Matrix matrix = new Matrix(3,3);
        MyVector myVector = new MyVector(3);
        matrix.set(0,0,4.0);
        matrix.set(0,1,-2.0);
        matrix.set(0,2,1.0);
        matrix.set(1,0,8.0);
        matrix.set(1,1,-1.0);
        matrix.set(1,2,1.0);
        matrix.set(2,0,1.0);
        matrix.set(2,1,3.0);
        matrix.set(2,2,-1.0);

        System.out.println("Example matrix");
        matrix.printMatrx();

        myVector.set(0,-4.0);
        myVector.set(1,-2.0);
        myVector.set(2,6.0);
        System.out.println("Example vector");
        myVector.printVector();

        Matrix matrix1 = new Matrix(3,3);
        for(int i=0;i<3;++i){
            for(int j=0;j<3;++j){
                matrix1.set(i,j,2.0);
            }
        }

        System.out.println("Add 3 to matrix");
        Matrix addmatrix = matrix.add(3.0);
        addmatrix.printMatrx();

        System.out.println("Paraller Add 3 to matrix");
        addmatrix = matrix.parallerAdd(3.0);
        addmatrix.printMatrx();

        System.out.println("Sub 3 to matrix");
        addmatrix = matrix.sub(3.0);
        addmatrix.printMatrx();

        System.out.println("Paraller Sub 3 to matrix");
        addmatrix = matrix.parallerSub(3.0);
        addmatrix.printMatrx();

        System.out.println("Multiply 3 to matrix");
        addmatrix = matrix.mutiply(3.0);
        addmatrix.printMatrx();

        System.out.println("Paraller multiply 3 to matrix");
        addmatrix = matrix.parallerMutiply(3.0);
        addmatrix.printMatrx();

        System.out.println("Add matrix1 to matrix");
        addmatrix = matrix.add(matrix1);
        addmatrix.printMatrx();

        System.out.println("Paraller Add matrix1 to matrix");
        addmatrix = matrix.parallerAdd(matrix1);
        addmatrix.printMatrx();

        System.out.println("Sub matrix1 to matrix");
        addmatrix = matrix.sub(matrix1);
        addmatrix.printMatrx();

        System.out.println("Paraller sub matrix1 to matrix");
        addmatrix = matrix.parallerSub(matrix1);
        addmatrix.printMatrx();

        System.out.println("Multiply matrix1 to matrix");
        addmatrix = matrix.multiply(matrix1);
        addmatrix.printMatrx();

        System.out.println("Paraller multiply matrix1 to matrix");
        addmatrix = matrix.parallerMultiply(matrix1);
        addmatrix.printMatrx();

        System.out.println("Multiply myvector to matrix");
        MyVector myVector1 = matrix.multily(myVector);
        myVector1.printVector();

        System.out.println("Paraller multiply myvector to matrix");
        myVector1 = matrix.parallerMultily(myVector);
        myVector1.printVector();

        System.out.println("Inner product myVector and myVector1");
        Double d = myVector.innderProduct(myVector1);
        System.out.println(d);

        System.out.println("Paraller Inner product myVector and myVector1");
        d = myVector.parallerInnderProduct(myVector1);
        System.out.println(d);

        System.out.println("Outer product myVector and myVector1");
        addmatrix = myVector.outerProduct(myVector1);
        addmatrix.printMatrx();

        System.out.println("Paraller Outer product myVector and myVector1 // don't work always : (");
        addmatrix = myVector.parallerOuterProduct(myVector1);
        addmatrix.printMatrx();


        System.out.println("\nBonus!");
        System.out.println("Solve linear problem usign iterate method: Conjugate residual method : )");
        System.out.println("Don't work for all linear problem, but we can solve linear problem for not square matrix");
        //Conjugate conjugate = new Conjugate(matrix,myVector);
        MyVector x = Conjugate.clc(matrix,myVector);
        x.printVector();


    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("He||o in program\nChose what u want\n1.Demo of program\n2.Read Matrix By Hand\n3.Read Vector\n4.Read From File");
        int info;
        info=s.nextInt();
        switch (info) {
            case 1:
                demo();
                break;
            case 2:
                readByHand();
                break;
            case 3:
                readVektor();
                break;
            case 4:
                readFromFile();
                break;
            default:
                break;
        }
    }
}