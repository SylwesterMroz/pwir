package com.company;

public class Conjugate {
    private Matrix A;
    private MyVector b;
    public Conjugate(Matrix A,MyVector b){
        this.A = A;
        this.b = b;
    }

    public static MyVector clc(Matrix A, MyVector b){

        MyVector x = new MyVector(b.getSize());
        for(int i=0;i<b.getSize();++i){
            x.set(i,1.0);
        }

        MyVector s = A.multily(x);
        MyVector r = x.sub(s);
        MyVector p = r;
        MyVector apk = A.multily(p);
        Matrix B = A.transpose();

        for(int k=0;k<2;++k){
            MyVector atr = B.multily(r);
            Double atrr = atr.innderProduct(r);
            Double apkp = apk.innderProduct(apk);
            Double alpha = atrr/apkp;
            x = x.add(p.multiply(alpha));
            r = r.sub(apk.multiply(alpha));
            atr = B.multily(r);
            Double atrr2 = atr.innderProduct(r);
            Double beta = atrr2/atrr;
            p=r.add(p.multiply(beta));
            apk = A.multily(r).add(apk.multiply(beta));
        }
        return x;
    }

    public MyVector calc(){
        MyVector x = new MyVector(b.getSize());
        for(int i=0;i<b.getSize();++i){
            x.set(i,1.0);
        }

        MyVector s = A.multily(x);
        MyVector r = x.sub(s);
        MyVector p = r;
        MyVector apk = A.multily(p);
        Matrix B = A.transpose();

        for(int k=0;k<2;++k){
            MyVector atr = B.multily(r);
            Double atrr = atr.innderProduct(r);
            Double apkp = apk.innderProduct(apk);
            Double alpha = atrr/apkp;
            x = x.add(p.multiply(alpha));
            r = r.sub(apk.multiply(alpha));
            atr = B.multily(r);
            Double atrr2 = atr.innderProduct(r);
            Double beta = atrr2/atrr;
            p=r.add(p.multiply(beta));
            apk = A.multily(r).add(apk.multiply(beta));
        }
        return x;
    }
}