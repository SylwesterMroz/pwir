package com.company;

import java.util.concurrent.RecursiveTask;

/**
 * Created by Ja on 20.01.2016.
 */
public class RecusiveSum extends RecursiveTask<Double> {
    RecusiveSum(Double[] array,int start,int end){
        this.array = array;
        this.start = start;
        this.end = end;
    }
    private final Double[] array;
    private final int start,end;
    @Override
    protected Double compute() {
        if((end - start) == 1){
            return array[start] + array[end];
        }
        if(end == start){
            return array[start];
        }
        RecusiveSum r1 = new RecusiveSum(array,start,(end + start)/2);
        r1.fork();
        RecusiveSum r2 = new RecusiveSum(array,(end + start)/2 + 1,end);
        return  r2.compute() + r1.join();
    }
}
