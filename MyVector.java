package com.company;

import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Ja on 19.01.2016.
 */
public class MyVector {
    private final Semaphore semaphore = new Semaphore(1);
    public MyVector(int s){
        size = s;
        data = new Double[s];
        for(int i=0;i<data.length;++i){
            data[i] = new Double(0.0);
        }
    }

    public Double get(int index){
        if(index<0 || index>=data.length){
            throw new IndexOutOfBoundsException("Wrong index of vector");
        }
        return data[index];
    }

    public Double parralerGet(int index){
        if(index<0 || index>=data.length){
            throw new IndexOutOfBoundsException("Wrong index of vector");
        }
        semaphore.acquireUninterruptibly();
        Double c;
        try{
            c = data[index];
        }finally {
            semaphore.release();
        }
        return c;
    }

    public void parralerSet(int index,Double value){
        if(index<0 || index>=data.length){
            throw new IndexOutOfBoundsException("Wrong index of vector");
        }
        semaphore.acquireUninterruptibly();
        try {
            data[index] = value;
        }finally {
            semaphore.release();
        }
    }

    public void set(int index,Double value) {
        if (index < 0 || index >= data.length) {
            throw new IndexOutOfBoundsException("Wrong index of vector");
        }
        data[index] = value;
    }


    public MyVector add(Double c){
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.set(i,data[i]+c);
        }
        return a;
    }

    public MyVector parallerAdd(Double c){
        Adder[] adders = new Adder[data.length];
        for(int i=0;i<data.length;++i){
            adders[i] = new Adder(data[i],c);
            adders[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                adders[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = adders[i].get();
        }
        return a;
    }

    public MyVector add(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<size;++i){
            a.data[i]=data[i]+B.get(i);
        }
        return a;
    }

    public MyVector parallerAdd(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Adder[] adders = new Adder[data.length];
        for(int i=0;i<data.length;++i){
            adders[i] = new Adder(data[i],B.parralerGet(i));
            adders[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                adders[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = adders[i].get();
        }
        return a;
    }

    public MyVector sub(Double c){
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = data[i]-c;
        }
        return a;
    }

    public MyVector parallerSub(Double c){
        Substracter[] substracters = new Substracter[data.length];
        for(int i=0;i<data.length;++i){
            substracters[i] = new Substracter(data[i],c);
            substracters[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                substracters[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = substracters[i].get();
        }
        return a;
    }

    public MyVector sub(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<size;++i){
            a.data[i]=data[i]-B.get(i);
        }
        return a;
    }

    public MyVector parallerSub(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Substracter[] substracters = new Substracter[data.length];
        for(int i=0;i<data.length;++i){
            substracters[i] = new Substracter(data[i],B.parralerGet(i));
            substracters[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                substracters[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = substracters[i].get();
        }
        return a;
    }

    public MyVector multiply(Double c){
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = data[i]*c;
        }
        return a;
    }

    public MyVector parralerMultiply(Double c){
        Multipler[] multiplers = new Multipler[data.length];
        for(int i=0;i<data.length;++i){
            multiplers[i] = new Multipler(c,data[i]);
            multiplers[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                multiplers[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = multiplers[i].get();
        }
        return a;
    }

    public MyVector multiply(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<size;++i){
            a.data[i]=data[i]*B.get(i);
        }
        return a;
    }

    public MyVector parallerMultiply(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Multipler[] multiplers = new Multipler[data.length];
        for(int i=0;i<data.length;++i){
            multiplers[i] = new Multipler(B.parralerGet(i),data[i]);
            multiplers[i].start();
        }
        for(int i=0;i<data.length;++i){
            try {
                multiplers[i].join();
            }catch (InterruptedException e){}
        }
        MyVector a = new MyVector(data.length);
        for(int i=0;i<data.length;++i){
            a.data[i] = multiplers[i].get();
        }
        return a;
    }

    public double parallerInnderProduct(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Double[] doubles = new Double[size];
        Multipler[] multiplers = new Multipler[size];
        for(int i=0;i<size;++i){
            multiplers[i] = new Multipler(data[i],B.parralerGet(i));
            multiplers[i].start();
        }
        for(int i=0;i<size;++i){
            try {
                multiplers[i].join();
            }catch (InterruptedException e){}
            doubles[i] = multiplers[i].get();
        }
        Double s=0.0;
        final int numOfThread = Runtime.getRuntime().availableProcessors();
        final ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThread);
        s = forkJoinPool.invoke(new RecusiveSum(doubles,0,size -1));
        return s;
    }

    public double paraller2InnderProduct(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Double[] doubles = new Double[size];
        for(int i=0;i<size;++i) {
            doubles[i] = data[i]* B.parralerGet(i);
        }
        Double s=0.0;
        final int numOfThread = Runtime.getRuntime().availableProcessors();
        final ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThread);
        s = forkJoinPool.invoke(new RecusiveSum(doubles,0,size -1));
        return s;
    }

    public double innderProduct(MyVector B){
        if(size != B.size){
            throw new IndexOutOfBoundsException("Diferent sizes of vectors");
        }
        Double s=0.0;
        for(int i=0;i<size;++i){
            s+=data[i]*B.get(i);
        }
        return s;
    }

    public Matrix outerProduct(MyVector B){
        Matrix matrix = new Matrix(size,B.size);
        for(int i=0;i<size;++i){
            for(int j=0;j<B.size;++j){
                matrix.set(i,j,data[i]*B.get(j));
            }
        }
        return matrix;
    }

    public Matrix parallerOuterProduct(MyVector B){
        Matrix matrix = new Matrix(size,B.size);
        MyVector A = this;
        final int numOfThread = Runtime.getRuntime().availableProcessors();


        ExecutorService executorService = new ThreadPoolExecutor(numOfThread, size*B.size, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()); //Executors.newFixedThreadPool(nThread);
        //ReentrantLock lock = new ReentrantLock();
        for(int i=0;i<A.size;++i){
            for(int j=0;j<B.size;++j){
                final int ii=i,jj=j;
                executorService.submit(() -> {
          //          lock.lock();
                 //   try {
                        matrix.parralerSet(ii, jj, A.parralerGet(ii) * B.parralerGet(jj));
               //     } finally {
             //           lock.unlock();
            //        }
                });
            }
        }
        executorService.shutdown();
        return matrix;
    }


    public void printVector(){
        System.out.print("{ ");
        for(int i=0;i<size;++i){
            System.out.print(data[i] + " ");
        }
        System.out.println("}");
    }

    private Double data[];

    public int getSize() {
        return size;
    }

    private int size;
}
